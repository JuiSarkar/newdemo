﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IComparableandIEquatable
{
    //Merge conglict for git clone
    public class Person:IComparable<Person>,IEquatable<Person>
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public int CompareTo(Person other)
        {
            return Name.CompareTo(other.Name);
        }
        public bool Equals(Person other)
        {          
                if (Age == other.Age && Name == other.Name)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            
        }
    }
}
