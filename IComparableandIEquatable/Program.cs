﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IComparableandIEquatable
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p1 = new Person()
            { Name = "Riya", 
              Age = 34 
            };
            Person p2 = new Person()
            { Name = "Riya", 
              Age = 34 
            };
            Person p3 = new Person()
            { Name = "Gita", 
              Age = 33 
            };
            Person p4 = new Person() 
            { Name = "Shreya", 
              Age = 26 
            };
            List<Person> people = new List<Person>
            { p1, p2, p3, p4 };

            var p1top2 = p1.Equals(p2);
            var p3top4 = p3.Equals(p4);
            Console.WriteLine($"Person p1 is equal to person p2 = {p1top2}");
            Console.WriteLine($"Person p3 is equal to person p4 = {p3top4}");
            Console.WriteLine("============================================================");
            people.Sort();
            foreach (var item in people)
            {
                Console.WriteLine(item.Name + " " + item.Age);
            }

            Console.ReadLine();
        }
    }
}
